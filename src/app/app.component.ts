import {Component, ViewChild} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  files: File[] = [];

  @ViewChild('arquivos', { static: true })
  filesInput: HTMLInputElement;

  onFileSelection({ files }: { files: FileList }) {
    for (let i = 0; i < files.length; i++) {
      this.addFile(files.item(i));
    }
  }

  addFile(file: File) {
    const oldFileIndex = this.files.findIndex(f => f.name === file.name);
    if (oldFileIndex >= 0) {
      this.files.splice(oldFileIndex, 1);
    }
    this.files.push(file);
    this.files.sort((fA, fB) => fA.name.localeCompare(fB.name));
  }

  removeFile(event: Event, index: number) {
    event.stopPropagation();
    event.preventDefault();
    this.files.splice(index, 1);
  }

  getFileIcon(file: File) {
    const iconsMap: { [kind: string]: string[] } = {
      audio: ['mp3'],
      video: ['mp4'],
      pdf: ['pdf'],
      image: ['png', 'jpg'],
      archive: ['zip'],
    };

    let fileIcon = 'fa-file';

    const fileNameParts = file.name.split('.');
    const fileExtension = fileNameParts[fileNameParts.length - 1];

    console.info(fileExtension);

    _.keysIn(iconsMap).forEach(kind => {
      if (iconsMap[kind].find(extension => extension === fileExtension)) {
        fileIcon = 'fa-file-' + kind + '-o';
      }
    });

    return fileIcon;
  }
}
